﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CueSearcher
{
    class itemsGrid
    {
        public string song { set; get; }
        public string album { set; get; }
        public string artist { set; get; }
        public string path { set; get; }
    }
}
