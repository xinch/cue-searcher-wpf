﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.IO;
using System.Collections.ObjectModel;
using System.Data;

using System.Diagnostics;
using System.Windows.Forms;

namespace CueSearcher
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ObservableCollection<itemsGrid> items = null;
        private const string TITLE_INDICATION = "TITLE";

        public MainWindow()
        {
            InitializeComponent();

            if (items == null)
            {
                items = new ObservableCollection<itemsGrid>();
                gridResult.ItemsSource = items;
            }
        }

        /// <summary>
        /// Метод обрабатывает cue файл - ищет в нем заданный songName
        /// </summary>
        /// <param name="filepath">путь к cue</param>
        private void findSong(string filepath)
        {
            char[] delimiters = { ' ', '\t' }; // определяем разделители

            string artist = new String(' ', 1);
            string album = new String(' ', 1);

            int titlefirst = 1;
            int flag = 0;
            StreamReader cuefile = new StreamReader(filepath, Encoding.Default);

            while (!cuefile.EndOfStream && flag == 0)
            {
                string substr = cuefile.ReadLine(); // читаем очередную строку из файла

                substr = substr.Trim(' ', '\t'); // режем лишнее из строки. не совсем раскурил как работает, потому делал методом тыка
                string[] words = substr.Split(delimiters); // пилим строку на слова

                if (words[0] == TITLE_INDICATION) //нашли заголовок 
                {
                    if (titlefirst == 1) // из-за особенностей строение cue, чтобы не переписывать переменную много раз за 1 файл
                    {
                        titlefirst = 0;
                        album = getStringFromSplit(words); // запоминаем название альбома
                    }
                    else if (string.Compare(getStringFromSplit(words), songNameTextBox.Text, true) == 0)
                    {
                        flag = 1; // бинго!!

                        substr = cuefile.ReadLine(); // читаем файл дальше
                        substr = substr.Trim(' ', '\t');
                        words = substr.Split(delimiters); // вычленяем имя испольнителя(группы)
                        artist = words[1].Trim('"');
                    }
                }
            }
            cuefile.Close();

            if (flag == 1) // если нашли совпадение
                items.Add(
                new itemsGrid() { song = songNameTextBox.Text, album = album, artist = artist, path = filepath }); // то пишем его в датагрид
        }

        /// <summary>
        /// разбить-то строку разбили, а вот обратно уже костыль
        /// </summary>
        /// <param name="words">substrings, из которых собирается строка</param>
        /// <returns>собранная из substrings строка</returns>
        private string getStringFromSplit(string[] words)
        {
            StringBuilder strout = new StringBuilder("");

            for (int i = 1; i < words.Length; i++)
            {
                strout.Append(words[i].Trim('"'));
                strout.Append(" ");
            }
            return strout.ToString().TrimEnd();
        }

        private void searchBtn_Click(object sender, RoutedEventArgs e)
        {
            List<string> dirs = FileSearcher.GetFilesRecursive(pathTextBox.Text); // ищем пути всех cue файлов  
            foreach (string paths in dirs)
                findSong(paths); // передаем пути на обработку
        }

        private void getPathBtn_Click(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog selectFolderDialog = new FolderBrowserDialog();
            selectFolderDialog.ShowDialog();
            pathTextBox.Text = selectFolderDialog.SelectedPath;
        }

        private void gridResult_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (e.OriginalSource == null) return;
            var row = ItemsControl.ContainerFromElement((System.Windows.Controls.DataGrid)sender, e.OriginalSource as DependencyObject) as DataGridRow;
            if (row != null)
            {
                var item = (itemsGrid)gridResult.Items[row.GetIndex()];
                    try
                    {
                        Process.Start(item.path);//пытаемся открыть файл программой по умолчанию
                    }
                    catch
                    {
                        //затычка
                    }
            }
        }
    }
}