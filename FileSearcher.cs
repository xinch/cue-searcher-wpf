﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace CueSearcher
{
    static class FileSearcher
    {
        private const string EXTENSION = "*.cue";

        public static List<string> GetFilesRecursive(string startpath)
        {
            // храним результаты в списке.
            List<string> dirlistout = new List<string>();

            //стек дирректорий 
            Stack<string> stack = new Stack<string>();

            // загоняем в стек "стартовую дирректорию"
            stack.Push(startpath);

            // продолжать, пока есть чо)
            while (stack.Count > 0)
            {
                string dir = stack.Pop();

                try
                {
                    // кидаем в список результатов все cue файлы в папке
                    dirlistout.AddRange(Directory.GetFiles(dir, EXTENSION));

                    // толкаем в стек все папки в текущей дирректории
                    foreach (string dn in Directory.GetDirectories(dir))
                        stack.Push(dn);
                }
                catch
                {
                    // если папку открыть не удалось, тупо затычка
                }
            }
            return dirlistout;
        }
    }
}
